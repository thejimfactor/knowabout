// misc util admin code
const {writeFile} = require('fs/promises')

const { initializeApp, applicationDefault, cert } = require('firebase-admin/app');
const { getFirestore, Timestamp, FieldValue } = require('firebase-admin/firestore');

//firestore
const serviceAccount = require('./keys/firebase1.json')

initializeApp({
  credential: cert(serviceAccount)
})
const db = getFirestore()
db.settings({ignoreUndefinedProperties:true}) // allow undefined in saved data

// get text for wordcloud
async function getKnowaboutsText(teamID, after, before){
  console.log('getKnowaboutsTimeframe')
  // timeframe
  const snapshot = await db.collectionGroup('knowabouts').where('created', '>=', after).where('created', '<=', before).where('active', '==', true).where('teamID', '==', teamID).get()
  
  // all
  //const snapshot = await db.collectionGroup('knowabouts').where('active', '==', true).where('teamID', '==', teamID).get()
  console.log(snapshot)
  console.log(snapshot.size)
  let knowaboutsText = ''
  const words = {}
  for(const knowabout of snapshot.docs){
    // build text file
    knowaboutsText += knowabout.data().text+"\n"

    // clean string https://stackoverflow.com/questions/4328500/how-can-i-strip-all-punctuation-from-a-string-in-javascript-using-regex
    const cleanedText = knowabout.data().text.replace(/[^\w\s]|_/g, "").replace(/\s+/g, " ")
    const splits = cleanedText.split(' ')
    for(const w of splits){
      if(words[w]){
        words[w] += 1
      }else{
        words[w] = 1
      }
    }
  }
  // build csv
  const wordsArray = []
  for(const word in words){
    wordsArray.push({word:word, count:words[word]})
  }

  console.log('wordsArray: ', wordsArray)
  wordsArray.sort((a,b)=>b.count - a.count)
  console.log('wordsArray: ', wordsArray)
  let csvString = 'Word,Count\n'
  for(const word of wordsArray){
    csvString += `${word.word},${word.count}\n`
  }

  await writeFile('./misc/knowabouts.txt', knowaboutsText)
  await writeFile('./misc/knowabouts.csv', csvString)

}

console.log('admin misc stuff');
(async () => {

  const oneWeekAgo = new Date()
  oneWeekAgo.setDate(oneWeekAgo.getDate()-7)
  const now = new Date()
  const teamID = 'TLN6SP0FM' // production
   //const teamID = 'T02SB19JVK6' // test
  //const installation = await app.receiver.installer.installationStore.fetchInstallation({teamId:teamID})
  const knowabouts = await getKnowaboutsText(teamID, oneWeekAgo, now)
  console.log('got knowabouts');
})();

