
const fetchP = import('node-fetch').then(mod => mod.default)
const fetch = (...args) => fetchP.then(fn => fn(...args))
const { initializeApp, applicationDefault, cert } = require('firebase-admin/app');
const { getFirestore, Timestamp, FieldValue } = require('firebase-admin/firestore');
const cron = require('node-cron')
const algoliasearch = require('algoliasearch')
const dayjs = require('dayjs')
const sheets = require('./sheets')


const search = algoliasearch('MU8WXJTLF3', process.env.ALGOLIA_API)
const index = search.initIndex('knowabouts')


const { App } = require('@slack/bolt');

// slack bolt
const app = new App({
  // token: process.env.SLACK_BOT_TOKEN,
  signingSecret: process.env.SLACK_SIGNING_SECRET,
  // socketMode: true,
  // appToken: process.env.SLACK_APP_TOKEN,
  // Socket Mode doesn't listen on a port, but in case you want your app to respond to OAuth,
  // you still need to listen on some port!
  // port: process.env.PORT || 3000
  signingSecret: process.env.SLACK_SIGNING_SECRET,
  clientId: process.env.SLACK_CLIENT_ID,
  clientSecret: process.env.SLACK_CLIENT_SECRET,
  stateSecret: 'my-secret',
  scopes:['channels:history', 'chat:write', 'commands', 'groups:history', 'im:history', 'mpim:history', 'channels:join', 'channels:manage', 'users:read', 'channels:read', 'groups:read', 'im:read', 'mpim:read'],
  installationStore:{
    storeInstallation: async (installation)=>{
      // console.log("installation: ", installation)
      const channelName = 'knowabouts'
      // store in firestore
      if (installation.isEnterpriseInstall && installation.enterprise !== undefined) {
        console.log(`creating and joining #${channelName} channel`)
        await createAndJoinKnowabouts(installation, channelName)
        console.log('inviting users')
        // don't await because it takes too long
        slackInstallInvite(installation)
        console.log('storing enterprise install')
        return await storeCredentials(installation.enterprise.id, installation)
      }
      if (installation.team !== undefined) {
        console.log(`creating and joining #${channelName} channel`)
        await createAndJoinKnowabouts(installation, channelName)
        console.log('inviting users')
        // don't await because it takes too long
        slackInstallInvite(installation)
        console.log('storing team install')
        return await storeCredentials(installation.team.id, installation)
      }
      throw new Error('Failed saving installation data to installationStore');
    },
    fetchInstallation: async (installQuery)=>{
      // console.log("installQuery: ", installQuery)
      if (installQuery.isEnterpriseInstall && installQuery.enterpriseId !== undefined) {
        return await getCredentials(installQuery.enterpriseId)
      }
      if (installQuery.teamId !== undefined) {
        return await getCredentials(installQuery.teamId)
      }
      throw new Error('Failed fetching installation')
    }
  }
});

//firestore
const serviceAccount = require('./keys/firebase1.json')

initializeApp({
  credential: cert(serviceAccount)
})
const db = getFirestore()
db.settings({ignoreUndefinedProperties:true}) // allow undefined in saved data


const commands = [
  {
    command:'search',
    regex:/search (.+)/gi,
    func:searchKnowabouts,
    results:{
      text:1
    },
    help:'`search_query` - will search all knowabouts'
  },
  {
    command:'leaderboardweek',
    regex:/^leaderboardweek/gi,
    func:sendLeaderboardLastWeek,
    results:{
      
    },
    help:'will generate a leaderboard'
  },
  {
    command:'top10',
    regex:/^top10/gi,
    func:slackGetTop10Knowabouts,
    results:{
      
    },
    help:'will find the most popular knowabouts in channel #knowabouts'
  },
  {
    command:'sent @user',
    regex:/^sent @([\w-_.]+)/gi,
    func:sentKnowabouts,
    results:{
      username:1 
    },
    help:'allows you to see knowabouts other people have written'
  },
  {
    command:'sent',
    regex:/^sent/gi,
    func:deleteSentKnowabouts,
    results:{
      
    },
    help:'allows you to delete your sent knowabouts'
  },
  {
    command:'match @user1 @user2',
    regex:/^match @([\w-_.]+) @([\w-_.]+)/gi,
    func:matchKnowabouts,
    results:{
      username1:1,
      username2:2 
    },
    isAdmin:true,
    help:'matches two people for a 1:1 and three knowabouts'
  },
  {
    command:'matchreminder',
    regex:/^matchreminder/gi,
    func:sendAllMatchReminders,
    results:{
      
    },
    isAdmin:true,
    help:'admin test command'
  },
  {
    command:'matches',
    regex:/^matches/gi,
    func:getMatchStats,
    results:{
      
    },
    isAdmin:true,
    help:'admin match stats'
  },
  {
    command:'beginmatch',
    regex:/^beginmatch/gi,
    func:beginMatch,
    results:{
      
    },
    isAdmin:true,
    help:'admin start weekly matches'
  },
  {
    command:'clearmatches',
    regex:/^clearmatches/gi,
    func:clearMatches,
    results:{
      
    },
    isAdmin:true,
    help:'admin resets all matches and held knowabouts are sent'
  },
]



app.action('button_click', async ({ body, ack, say }) => {
  // Acknowledge the action
  await ack();
  await say(`<@${body.user.id}> clicked the button`);
});

// app.message(/^help/, async ({ message, say }) => {
//   if(message.channel_type != 'im')
//     return
//   const helpMessage = getHelpMessage()
//   say(helpMessage)
// })

// process all DMs
// app.message(/^@[\w]+[\s]+[\w]/, async ({ message, say }) => {
// app.message(/^<@[\w]+>/, async ({ message, say }) => {
app.message(/.*/, async ({ message, say }) => {
  // check that the channel is direct message with knowabouts-bot
  if(message.channel_type != 'im')
    return

  console.log('message: ', message)
  const regex = /^<@([\w]+)>[\s]*(.*)/
  const result = regex.exec(message.text)
  console.log('regex result: ', result)
  const knowaboutUserID = result ? result[1] : ""
  const knowaboutText = result ? result[2] : ""
  const senderID = message.user
  const teamID = message.team
  const installation = await app.receiver.installer.installationStore.fetchInstallation({teamId:teamID})
  const userText = message.text.toLowerCase()


  // display help message
  if(userText.startsWith('help')){
    const helpMessage = getHelpMessage()
    return await say(helpMessage)
  }else if(!result){
    // if no match return early
    return await say(`Sorry I don’t know what you mean by: ${message.text}. Type \`help\` to see available commands.`)
  }

  // await say({text:`Knowabout not sent <@${knowaboutUserID}> : ${knowaboutText}`})
  // await say('no text set')
  await processKnowabout(knowaboutUserID, knowaboutText, teamID, senderID, knowaboutUserID, null,say, installation, result)
})

// accept button with knowaboutID was clicked
app.action(/^accept_[\w]+/, async ({ body, ack, say }) => {
  console.log('in accept button handler')
  await ack()
  console.log('accept_ body: ', body)
  console.log('body.actions[0].action_id: ', body.actions[0].action_id)
  // return early if it's an accept_reply click
  if(body.actions[0].action_id == 'accept_reply')
    return
  const splits = body.actions[0].action_id.split('_')
  const pendingID = splits[1]
  const channelID = splits[2] // TODO: deprecated
  const userID = body.user.id
  const teamID = body.user.team_id
  const installation = await app.receiver.installer.installationStore.fetchInstallation({teamId:teamID})

  const knowaboutDoc = await getKnowabout(userID, teamID, pendingID)
  // return early if it's already marked as active
  if(knowaboutDoc && knowaboutDoc.data().active)
    return

  await approveKnowabout(userID, teamID, pendingID)
  const slackResponse = await slackUpdateKnowaboutsChannel(userID, teamID, pendingID, channelID, installation)

  // replace approve/reject with a reply button 
  // 
  const permalink = await app.client.chat.getPermalink({
    token:installation.bot.token,
    channel:slackResponse.channel,
    message_ts:slackResponse.ts
  })

  await replaceButtonsApproveReject(body, permalink)
  // console.log("noButtonsBlocks: ", noButtonsBlocks)
  // await app.client.chat.update({
  //   token: process.env.SLACK_BOT_TOKEN,
  //   channel:userID,
  //   ts:body.message.ts,
  //   blocks:noButtonsBlocks
  // })
  
  await say('That knowabout has been approved!')

  // check if they've been subscribed to knowabouts
  const userDoc = await getUserFirestore(userID, teamID)
  console.log('------userDoc.data():', userDoc.data())
  if(!userDoc.data() || !userDoc.data().subscribed){
    const blocks = getBlocksSubscribe()
    say({blocks:blocks})
    await subscribeToKnowabouts(userID, teamID, installation)
  }

  // check if the've been piled before
  if(!userDoc.data() || !userDoc.data().pileon){
    await pileonUser(userID, teamID, installation)
  }

})

// reject button with knowaboutID was clicked
app.action(/^reject_[\w]+/, async ({ body, ack, say }) => {
  await ack()
  await removeButtonsFromApproveReject(body)
  await say(`That knowabout won't be used`);
})

// unsubscribe user from knowabouts currently
app.action(/^unsubscribe/, async ({ body, ack, say }) => {
  await ack()
  const userID = body.user.id
  const teamID = body.user.team_id
  const installation = await app.receiver.installer.installationStore.fetchInstallation({teamId:teamID})
  // remove buttons
  body.message.blocks = body.message.blocks.slice(0, body.message.blocks.length-1)
  console.log("-------unsubscribe new blocks: ", body.message.blocks)
  // TODO: remove buttons
  await unsubscribeToKnowabouts(userID, teamID, installation)
  await say("Okay, you've been unsubscribed")
  // await updateSlackMessageBlocks(body)
})
// delete button on self knowabouts list
app.action(/^delete_received_[\w]+/, async ({ body, ack, say }) => {
  await ack()
  console.log('delete_received_ body: ', body)
  console.log('body.actions[0].action_id: ', body.actions[0].action_id)
  const splits = body.actions[0].action_id.split('_')
  const knowaboutID = splits[2]
  const knowaboutPosition = splits[3]
  const userID = body.user.id
  const teamID = body.user.team_id
  // if(body.container.is_ephemeral){
  //   // slash command was used
  //   const headers = {
  //     "Content-Type":"application/json"
  //   }
  //   const params = {
  //     method: 'post',
  //     headers:headers,
  //     body:JSON.stringify({
  //       text:'this is ephemeral'
  //     })
  //   }
  //   console.log("overwriting the deleted knowabout with Removed, params: ", params)
  //   await fetch(body.response_url, params)
  //   return
  // }
  // const blocks = body.message.blocks
  // console.log("--------blocks: ", blocks)
  // console.log("--------blocks[knowaboutPosition]: ", blocks[knowaboutPosition])
  // // update removed block with Removed and checkmark
  // blocks[knowaboutPosition].text.text = 'Removed'
  // blocks[knowaboutPosition].accessory.text.text = ":white_check_mark:"
  // console.log("--------blocks[knowaboutPosition] update: ", blocks[knowaboutPosition])


  await disableKnowabout(userID, teamID, knowaboutID)
  const knowabouts = await getActiveKnowabouts(userID, teamID)

  const deleteBlocks = getBlocksDeleteList(knowabouts, userID)


  console.log("updating blocks")
  body.message = {blocks:deleteBlocks}
  await updateSlackMessageBlocks(body)
  // await say('knowabout deleted')
})

// delete button on sent knowabouts list
app.action(/^delete_sent_[\w]+/, async ({ body, ack, say }) => {
  await ack()
  console.log('delete_sent_ body: ', body)
  console.log('body.actions[0].action_id: ', body.actions[0].action_id)
  const splits = body.actions[0].action_id.split('_')
  const knowaboutUserID = splits[2]
  const knowaboutID = splits[3]
  const knowaboutPosition = splits[4]
  const senderID = body.user.id
  const teamID = body.user.team_id


  await disableKnowabout(knowaboutUserID, teamID, knowaboutID)
  const knowabouts = await getSentKnowabouts(senderID, teamID)

  const deleteBlocks = getBlocksSentDeleteList(knowabouts)

  body.message = {blocks:deleteBlocks}
  console.log('updating message blocks')
  await updateSlackMessageBlocks(body)
})

// handle search yes no
app.action(/^search_.+/, async ({ body, ack, say }) => {
  await ack()
  console.log('search_ body: ', body)
  console.log('body.actions[0].action_id: ', body.actions[0].action_id)
  const userID = body.user.id
  const teamID = body.user.team_id
  const splits = body.actions[0].action_id.split('_')
  const hitsPerPage = splits[3]
  const searchQuery = splits[4]
  console.log('splits: ', splits)
  const installation = await app.receiver.installer.installationStore.fetchInstallation({teamId:teamID})
  const knowaboutsChannelID = await slackGetChannelID('knowabouts', installation)
  const helpChannelID = await slackGetChannelID('help', installation)

  // send messages if desired
  if(splits[2] == 'yes'){
    await app.client.chat.postMessage({
      token:installation.bot.token,
      channel: knowaboutsChannelID,
      text: `<@${userID}> is looking for someone who knows about ${searchQuery}`
    })
    // TODO: send to channel help also
  }


  // remove the last block TODO: maybe only do some of this if it's an ephemeral
  // rerun search to replace ephemeral message
  // hitsPerPage will fetch more results automatically if set to higher number
  const results = await index.search(searchQuery, {
    advancedSyntax:true,
    hitsPerPage:hitsPerPage
  })
  const activeHits = results.hits.filter(h=>h.active)
  console.log("-----search_ search response: ", results)
  results.hits = activeHits
  let blocks = await getBlocksSearch(results, searchQuery)

  // remove notify buttons if they clicked one of them
  if(splits[1] == 'notify'){
    // console.log("old blocks: ", blocks)
    blocks = blocks.slice(0, blocks.length-1)
    // console.log("new blocks: ", blocks)
    // update body with new blocks may cause issues for non ephemeral replacing message object
  }
  body.message = {
    blocks:blocks
  }
  await updateSlackMessageBlocks(body)
})


// optin button was clicked 
app.action(/^optin_[\w]+/, async ({ body, ack, say }) => {
  await ack() 
  console.log('-----optin body: ', body)
  const triggerID = body.trigger_id
  const splits = body.actions[0].action_id.split('_')
  const yesSkip = splits[1]
  const optinID = splits[2] 
  const userID = body.user.id
  const teamID = body.user.team_id
  const installation = await app.receiver.installer.installationStore.fetchInstallation({teamId:teamID})
  // get user
  const user = await slackGetUserInfoByID(userID, installation)
  console.log('-------optin user info: ', user)

  // get topics
  const teamDoc = await db.collection('teams').doc(teamID).get()

  // open modal
  const modalView = getModalViewOptin(teamDoc.data().topics)
  await app.client.views.open({
    token:installation.bot.token,
    trigger_id:triggerID,
    view:modalView
  })
  // update optin flow
  await updateOptin(teamID, optinID, user, yesSkip)
})

// optin view submitted
app.view('optin_view', async ({ ack, body, view, client, logger }) => {
  await ack()
  console.log("-----view: ", view)
  const userID = body.user.id
  const teamID = body.user.team_id
  const installation = await app.receiver.installer.installationStore.fetchInstallation({teamId:teamID})
  // get user
  const user = await slackGetUserInfoByID(userID, installation)

  console.log("-----view topic choices: ", view.state.values.topic)
  const choices = []
  for(const value of view.state.values.topic.modal_optin_topics.selected_options){
    console.log(`choice: `, value)
    choices.push(value.value)
  }
  console.log('choices: ', choices)

  await saveOptinChoices(user, teamID, choices)
})

// match knowabouts reminder button was clicked
app.action(/^match_[\w]+/, async ({ body, ack, say }) => {
  await ack()
  const splits = body.actions[0].action_id.split('_')
  const okMeet = splits[1]
  const matchID = splits[2] 
  const userID = body.user.id
  const teamID = body.user.team_id

  if(okMeet == 'meet'){
    await updateMatchDidNotMeet(teamID, matchID)
  }

  await removeButtonsFromApproveReject(body)
  await say(`Thanks!`)
})

// listen for /ka or /knowabout
app.command('/knowabouts', async({ command, ack, respond }) =>{
  console.log("/knowabouts command received")
  await ack()
  console.log("acknowledged command.text: ", command.text)
  console.log("command: ", command)
  // console.log("command: ", command)
  // await respond(`msg:${command.text}`)
  // console.log("responded to")

  // get username in form /command @username some text here
  const regex = /((@([\w-_.\d]+))|(help))[\s]*([\s\S]*)/
  const result = regex.exec(command.text)
  console.log('regex result: ', result)


  // only grab regex match values if the right side of the | was matched
  const knowaboutUsername = (result&&!result[4]) ? result[3] : ""
  const knowaboutText = (result&&!result[4]) ? result[5] : ""
  const knowaboutHelp = (result&&!result[2]) ? result[1] : ""
  const teamID = command.team_id
  const channelID = command.channel_id

  const installation = await app.receiver.installer.installationStore.fetchInstallation({teamId:teamID})

  // search commands
  const foundCommand = findCommand(command.text)
  console.log("foundCommand: ", foundCommand)
  // call found command function with data and original slack command
  if(foundCommand){
    foundCommand.data.installation = installation
    return await foundCommand.func(foundCommand.data, command, respond)
  }

  // get userID for who was @'ed
  const knowaboutUserID = await slackGetUserID(knowaboutUsername, installation)
  console.log("knowaboutUserID: ", knowaboutUserID)

  await processKnowabout(knowaboutUserID, knowaboutText, teamID, command.user_id, knowaboutUsername, channelID, respond, installation, result, knowaboutHelp)
});

(async () => {
  // Start your app
  await app.start(process.env.PORT || 3004);

  console.log('4 Bolt app is running!');
})();


//
// helper functions
//

function findCommand(text){
  let foundCommand = null
  // console.log('command text: ', text)
  // iterate over commands to find match
  for(const command of commands){
    // console.log('command: ', command)
    const result = command.regex.exec(text)
    command.regex.lastIndex=0 // reset regex
    console.log('find command result: ', result)
    if(result){
      foundCommand = command
      foundCommand.data = {}
      // iterate over command.results to populate variables
      const keys = Object.keys(command.results)
      for(const key of keys){
        foundCommand.data[key] = result[foundCommand.results[key]]
      }
      return foundCommand
    }
  }
  return null
}
async function processKnowabout(knowaboutUserID, knowaboutText, teamID, senderID, knowaboutUsername, channelID, respond, installation, regexResult, help=null){
  if(knowaboutUserID == senderID && knowaboutText ){
    // no self knowabouts
    await respond(`Whoops… you can't give yourself a Knowabout. Share the love and give a Knowabout to someone else!`)
  }else if(help == 'help'){
    const helpMessage = getHelpMessage()
    await respond(helpMessage)
  }else if(knowaboutUserID && !knowaboutText){
    // return all knowabouts about user
    const knowabouts = await getActiveKnowabouts(knowaboutUserID, teamID)
    let knowaboutsString = `Knowabouts for <@${knowaboutUserID}>`
    const knowaboutsMessage = {}
    knowaboutsString = '' // no header for now
    if(knowabouts.length > 0 && knowaboutUserID == senderID){
      // self knowabouts list
      const deleteBlocks = getBlocksDeleteList(knowabouts, knowaboutUserID)
      knowaboutsString = 'this is a self knowabouts list with delete buttons'
      knowaboutsMessage.blocks = deleteBlocks
    }else if(knowabouts.length > 0){
      // other person knowabouts list
      for(const knowabout of knowabouts){
        const dayTS = dayjs(knowabout.data().created.toDate())
        const dayFormat = dayTS.format('MMM D, YYYY')

        knowaboutsString += `${dayFormat} <@${knowabout.data().senderID}> gave <@${knowaboutUserID}> a Knowabout: "${knowabout.data().text}"\n`
      }
    }else{
      knowaboutsString = `<@${knowaboutUserID}> hasn't received any Knowabouts yet. Tell us what you know about <@${knowaboutUserID}> with */knowabouts*`
    }
    knowaboutsMessage.text = knowaboutsString
    // await respond(knowaboutsString)
    await respond(knowaboutsMessage)

  }else if(regexResult == null){
    // give command help for naked /knowabout
    await respond(`How to use Knowabouts: */knowabouts @username [message]*`)

  }else if(!knowaboutUserID){
    await respond(`Please check that @${knowaboutUsername} is a username`)
  }else{
    // store new pending knowabout
    // get pending knowabout id
    const pendingID = await newKnowabout(knowaboutUserID, knowaboutUsername, teamID, knowaboutText, senderID)

    // check if they're in an active match sender and receiver
    const matchDoc = await getMatch(knowaboutUserID, senderID, teamID)
    if(matchDoc){
      // update match
      const result = await updateMatchSent(knowaboutUserID, senderID, teamID)
      if(result && result.sendAll){
        // send all knowabouts for both matchees
        await sendAllMatchKnowabouts(knowaboutUserID, senderID, teamID, installation)
        return await sendAllMatchKnowabouts(senderID, knowaboutUserID, teamID, installation)
      }else if(result && result.allSent){
        // continue as this match meets knowabouts written requirements

      }else{
        // return early if in an active match 
        return await respond(`You are in an active match with <@${knowaboutUserID}>`)
      }

    }

    const acceptRejectBlocks = getBlocksAcceptReject(senderID, knowaboutText, pendingID, channelID)

    // send accept/reject knowabout message
    await app.client.chat.postMessage({
      token:installation.bot.token,
      channel:knowaboutUserID,
      // text:`<@${command.user_id}> gave you a Knowabout\n${knowaboutText}`
      blocks:acceptRejectBlocks,
      text:`<@${senderID}> gave you a Knowabout:\n${knowaboutText}`
    })
    // send confirmation to sender
    await respond(`Thank you for sending a Knowabout!`)
  }
}

function getBlocksAcceptReject(senderID, knowaboutText, pendingID, channelID){
  const blocks = [
    {
      type:"section",
      text:{
        type:"mrkdwn",
        text:`<@${senderID}> gave you a Knowabout:\n${knowaboutText}`
      }
    },
    {
      type:"actions",
      elements:[
        {
          type:'button',
          text:{
            type:'plain_text',
            text:'Accept'
          },
          style:"primary",
          action_id:`accept_${pendingID}_${channelID}`
        },
        {
          type:'button',
          text:{
            type:'plain_text',
            text:'Reject'
          },
          action_id:`reject_${pendingID}_${channelID}`
        }
      ]
    }
  ]
  return blocks
}

// deprecated
function getBlocksAcceptReply(permalink){
  const blocks = [
    {
      type:"section",
      text:{
        type:"mrkdwn",
        text:`That knowabout has been approved!`
      }
    },
    {
      type:"actions",
      elements:[
        {
          type:'button',
          text:{
            type:'plain_text',
            text:'Reply'
          },
          style:"primary",
          url:permalink.permalink,
          action_id:`accept_reply`
        }
      ]
    }
  ]
  return blocks
}

// delete received
function getBlocksDeleteList(knowabouts, knowaboutUserID){
  const blocks = []
  let count = 0
  for(const knowabout of knowabouts){
    console.log("knowaboutID: ", knowabout.id)
    const dayTS = dayjs(knowabout.data().created.toDate())
    const dayFormat = dayTS.format('MMM D, YYYY')
    blocks.push(
      {
        type: "section",
        text: {
          type: "mrkdwn",
          text:`${dayFormat} <@${knowabout.data().senderID}> gave <@${knowaboutUserID}> a Knowabout: "${knowabout.data().text}"`
        },
        accessory: {
          type: "button",
          text: {
            type: "plain_text",
            text: ":x:",
            emoji: true
          },
          action_id: "delete_received_"+knowabout.id+"_"+count++,
          style:'danger',
          confirm:{
            title:{
              type:'plain_text',
              text:"Are you sure?"
            },
            text:{
              type:'mrkdwn',
              text:"This will delete this knowabout"
            },
            confirm:{
              type:'plain_text',
              text:"Yes"
            },
            deny:{
              type:'plain_text',
              text:"Cancel"
            }
          }
        }
      }
    )
  }
  return blocks
}


// delete sent
function getBlocksSentDeleteList(knowabouts){
  const blocks = []
  let count = 0
  for(const knowabout of knowabouts){
    console.log("knowaboutID: ", knowabout.id)
    const splits = knowabout.ref.path.split('/')
    const knowaboutUserID = splits[3]
    blocks.push(
      {
        type: "section",
        text: {
          type: "mrkdwn",
          text:`<@${knowabout.data().senderID}> gave <@${knowaboutUserID}> a Knowabout: "${knowabout.data().text}"`
        },
        accessory: {
          type: "button",
          text: {
            type: "plain_text",
            text: ":x:",
            emoji: true
          },
          action_id: `delete_sent_${knowaboutUserID}_${knowabout.id}_${count++}`,
          style:'danger',
          confirm:{
            title:{
              type:'plain_text',
              text:"Are you sure?"
            },
            text:{
              type:'mrkdwn',
              text:"This will delete this knowabout"
            },
            confirm:{
              type:'plain_text',
              text:"Yes"
            },
            deny:{
              type:'plain_text',
              text:"Cancel"
            }
          }
        }
      }
    )
  }
  return blocks
}

function getBlocksSearch(searchResults, query){
  let blocks = null
  const increment = 5

  if(searchResults.hits.length == 0){
    blocks = [
      {
        type:"section",
        text:{
          type:"mrkdwn",
          text:`We found zero results. Should we ask the community to find someone who knows about ${query}?`
        }
      },
      {
        type:"actions",
        elements:[
          {
            type:'button',
            text:{
              type:'plain_text',
              text:'Yes'
            },
            style:"primary",
            action_id:`search_notify_yes_${searchResults.hitsPerPage}_${query}`
          },
          {
            type:'button',
            text:{
              type:'plain_text',
              text:'No'
            },
            action_id:`search_notify_no_${searchResults.hitsPerPage}_${query}`
          }
        ]
      }
    ]
  }else{
    blocks = [
      {
        type:"section",
        text:{
          type:"mrkdwn",
          text:`Knowabouts search result:\n\n`
        }
      },
      {
        type:"actions",
        elements:[
          {
            type:'button',
            text:{
              type:'plain_text',
              text:'See more'
            },
            style:"primary",
            action_id:`search_more_filler_${searchResults.hitsPerPage+5}_${query}`
          }
        ]
      },
      {
        "type": "divider"
      },
      {
        type:"section",
        text:{
          type:"mrkdwn",
          text:`Should we ask the community to find more people who know about ${query}?`
        }
      },
      {
        type:"actions",
        elements:[
          {
            type:'button',
            text:{
              type:'plain_text',
              text:'Yes'
            },
            style:"primary",
            action_id:`search_notify_yes_${searchResults.hitsPerPage}_${query}`
          },
          {
            type:'button',
            text:{
              type:'plain_text',
              text:'No'
            },
            action_id:`search_notify_no_${searchResults.hitsPerPage}_${query}`
          }
        ]
      }
    ]
    // const promptText = blocks[0].text.text
    // blocks[0].text.text = "Knowabouts search result:\n"
    for(hit of searchResults.hits){
      // get slack userID
      const splits = hit.path.split('/')
      const knowaboutUserID = splits[3]

      // bold search words
      const boldSplits = hit.text.split(' ')
      const lowerBoldSplits = hit.text.toLowerCase().split(' ')
      const cleanedQuery = query.toLowerCase().replace('"', '')
      const querySplits = cleanedQuery.split(' ')
      for(const q of querySplits){
        for(i = 0; i < boldSplits.length; i++){
          // check if item contains split q then bold item as long as it's not bolded already
          if(lowerBoldSplits[i].includes(q) && !boldSplits[i].includes('*'))
            boldSplits[i] = `*${boldSplits[i]}*`
        }
      }
      const boldedText = boldSplits.join(' ')
      blocks[0].text.text += `<@${hit.senderID}> gave <@${knowaboutUserID}> a Knowabout: "${boldedText}"\n\n`
    }
    // if no more results remove see more button and show end of results
    if(searchResults.page+1 == searchResults.nbPages){
      blocks.splice(1,1, {
        type:"section",
        text:{
          type:"mrkdwn",
          text:`End of results`
        }
      })
    }
    // blocks[0].text.text += "\n\n"+promptText
  }
  return blocks
}

function getBlocksSubscribe(){
  const blocks = [
    {
      type: "section",
      text: {
        type: "mrkdwn",
        text:`Great, we’ve subscribed you to the #knowabouts channel where you can keep up with the conversation on your Knowabout as well as read Knowabouts for others.`
      },
    }
  ]
  return blocks
}

function getBlocksInstallInvite(){
  const blocks = [{
    type: "section",
    text: {
      type: "mrkdwn",
      text:`Hey, our community is using Knowabouts to share and learn what we know about each other! Join the #knowabouts channel to learn more about members you might want to get to know!`
    },
    accessory: {
      type: "button",
      text: {
        type: "plain_text",
        text: "Let me join the party! :partying_face:",
        emoji: true
      },
      action_id: "join_knowabouts",
      style:"primary"
    }
  }]
  return blocks
}

function getBlocksTop10(messages){
}
function getBlocksNoMatchKnowabouts(targetID, matchID){
  const blocks = [
    {
      type:"section",
      text:{
        type:"mrkdwn",
        text:`We don’t have your Knowabouts for <@${targetID}>. Write them now?`
      }
    },
    {
      type:"actions",
      elements:[
        {
          type:'button',
          text:{
            type:'plain_text',
            text:'Ok'
          },
          style:"primary",
          action_id:`match_ok_${matchID}`
        },
        {
          type:'button',
          text:{
            type:'plain_text',
            text:"We didn't get to meet yet"
          },
          action_id:`match_meet_${matchID}`
        }
      ]
    }
  ]
  return blocks
}
function getBlocksOptin(optinID){
  const blocks = [
    {
      type:"section",
      text:{
        type:"mrkdwn",
        text:`Are you in for 1:1 curated Knowabouts matches this week?`
      }
    },
    {
      type:"actions",
      elements:[
        {
          type:'button',
          text:{
            type:'plain_text',
            text:'Yes I\'m in :conga_line_parrot:',
            emoji: true
          },
          style:"primary",
          action_id:`optin_yes_${optinID}`
        },
        {
          type:'button',
          text:{
            type:'plain_text',
            text:"Skip this week",
            emoji: true
          },
          action_id:`optin_skip_${optinID}`
        }
      ]
    }
  ]
  return blocks
}

function getBlocksOptinUsers(docData){
  const blocks = [
    {
      type:"section",
      text:{
        type:"mrkdwn",
        text:`Is ready to 1to1: `
      }
    },
    {
      type:"section",
      text:{
        type:"mrkdwn",
        text:`Is skipping this week: `
      }
    }
  ]
  for(const user of docData.yes){
    blocks[0].text.text = blocks[0].text.text + `<@${user}> `
  }
  for(const user of docData.skip){
    blocks[1].text.text = blocks[1].text.text + `<@${user}> `
  }
  return blocks
}

function getModalViewOptin(topics){
  // NOTE: block_id must be unique between updates, so change it on update
  const view = {
    type: 'modal',
    // View identifier
    callback_id: 'optin_view',
    title: {
      type: 'plain_text',
      text: "Choose some topics!"
    },
    submit: {
      type: "plain_text",
      text: "Submit",
      emoji: true
    },
    close: {
      type: "plain_text",
      text: "Cancel",
      emoji: true
    },
    blocks:[
      {
        type: "input",
        block_id: "topic",
        element: {
          type: "multi_static_select",
          placeholder: {
            type: "plain_text",
            text: "Choose category",
            emoji: true
          },
          options: [
            {
              text: {
                type: "plain_text",
                text: "*this is plain_text text*",
                emoji: true
              },
              value: "value-0"
            },
          ],
          action_id: "modal_optin_topics"
        },
        label: {
          type: "plain_text",
          text: "Choose topics",
          emoji: true
        }
      }
    ]
  }
  const options = []
  for(const topic of topics){
    const lowerClean = topic.toLowerCase().replace('/', '\\')
    const option = {
      text: {
        type: "plain_text",
        text: topic,
        emoji: true
      },
      value: lowerClean
    }
    options.push(option)
  }
  view.blocks[0].element.options = options
  console.log('------view object:', view)
  //console.log('------view options object:', view.blocks[0].element.options)
  return view
}
function getHelpMessage(){
  const helpMessage = `\nSend a Knowabout: \`/knowabouts @person something nice about them\`\n
View someone's knowabouts: \`/knowabouts @person\`\n
View and delete your knowabouts: \`/knowabouts @yourname\`\n
Search all knowabouts with \`/knowabouts search\`\n
View most popular Knowabouts \`/knowabouts top10\`\n
Hint: You can also use the Knowabouts-bot page without needing the /knowabouts slash command\n
Send a Knowabout on the bot page with: \`@person something nice about them\`\n
View someone's knowabouts on the bot page with: \`@person\`\n`
  return helpMessage
}

async function removeButtonsFromApproveReject(body){
  // update message by removing accept reject buttons
  const blocks = body.message.blocks
  // console.log("blocks: ", blocks)
  const noButtonsBlocks = blocks.slice(0,1)
  const headers = {
    "Content-Type":"application/json"
  }
  const params = {
    method: 'post',
    headers:headers,
    body:JSON.stringify({
      blocks:noButtonsBlocks
    })
  }
  console.log("overwriting the buttons with POSTing a new message, params: ", params)
  await fetch(body.response_url, params)
}
async function replaceButtonsApproveReject(body, permalink){
  // update message by replacing accept reject buttons
  const blocks = body.message.blocks
  blocks[1] = {
    type:"actions",
    elements:[
      {
        type:'button',
        text:{
          type:'plain_text',
          text:'Reply'
        },
        style:"primary",
        url:permalink.permalink,
        action_id:`accept_reply`
      }
    ]
  
  }
  const headers = {
    "Content-Type":"application/json"
  }
  const params = {
    method: 'post',
    headers:headers,
    body:JSON.stringify({
      blocks:blocks
    })
  }
  console.log("overwriting the buttons with POSTing a new message, params: ", params)
  await fetch(body.response_url, params)
}

// takes an updated body and body.message.blocks and updates it in slack
async function updateSlackMessageBlocks(body){
  // update message by removing accept reject buttons
  // const blocks = body.message.blocks
  // console.log("blocks: ", blocks)
  // const noButtonsBlocks = blocks.slice(0,1)
  const headers = {
    "Content-Type":"application/json"
  }
  const params = {
    method: 'post',
    headers:headers,
    body:JSON.stringify({
      blocks:body.message.blocks
    })
  }
  console.log('updating blocks via: ', body.response_url)
  await fetch(body.response_url, params)
}

//
// slack functions
//

async function createAndJoinKnowabouts(installation, channelName){
  try{
    const createChannel = await app.client.conversations.create({
      token:installation.bot.token,
      name:channelName
    })
    console.log('createChannel: ', createChannel)
  }catch(err){
    console.log(`error creating channel ${channelName}: `, err)
    // TODO: try to join channel
    try{
      const channelID = await slackGetChannelID(channelName, installation)
      console.log(`joining channel ${channelName}, id: ${channelID}`)
      await app.client.conversations.join({
        token:installation.bot.token,
        channel:channelID
      })
    }catch(err2){
      console.log(`unable to join channel ${channelName}: `, err2)
      await app.client.chat.postMessage({
        token:installation.bot.token,
        channel:installation.user.id,
        text:`Knowabouts bot was unable to join #${channelName}. Please make sure this channel is created and the knowabouts bot is invited to it!`
      })
    }
  }
  // await app.client.conversations.join({
  //   token:installation.bot.token,
  //   channel:createChannel.channel.id
  // })
}

async function slackInstallInvite(installation){
  // message every user
  
  let users = {
    response_metadata:{
      next_cursor:'initial'
    }
  }
  const blocks = getBlocksInstallInvite()

  while(users.response_metadata.next_cursor){
    const params = {
      token:installation.bot.token
    }
    // paginate
    if(users.response_metadata.next_cursor != 'initial')
      params.cursor = users.response_metadata.next_cursor

    users = await app.client.users.list(params)

    for(const user of users.members){
      await app.client.chat.postMessage({
        token:installation.bot.token,
        channel:user.id,
        blocks:blocks
      })
    }
  }
}


// get userID given a text @name
async function slackGetUserID(username, installation){
  username = username.toLowerCase()
  // TODO: paginate for more than 1000 users. find out how many users are pulled down by default
  // paginate through users
  let users = {
    response_metadata:{
      next_cursor:'initial'
    }
  }

  let userID = null
  while(users.response_metadata.next_cursor){
    const params = {
      token:installation.bot.token,
      limit:500, // for testing in low user environments
    }
    // paginate
    if(users.response_metadata.next_cursor != 'initial')
      params.cursor = users.response_metadata.next_cursor

    users = await app.client.users.list(params)

    for(const user of users.members){
      // console.log(`id: ${user.id}, name: ${user.name}`)
      if(user.name == username)
        userID = user.id
    }
  }

  return userID
}

// get user info given an ID
async function slackGetUserInfoByID(userID, installation){
  // paginate through users
  let users = {
    response_metadata:{
      next_cursor:'initial'
    }
  }

  let gotUser = null
  while(users.response_metadata.next_cursor){
    const params = {
      token:installation.bot.token,
      limit:500, // for testing in low user environments
    }
    // paginate
    if(users.response_metadata.next_cursor != 'initial')
      params.cursor = users.response_metadata.next_cursor

    users = await app.client.users.list(params)

    for(const user of users.members){
      // console.log(`id: ${user.id}, name: ${user.name}`)
      if(user.id == userID)
        gotUser = user
    }
  }

  return gotUser
}

// message a user or channel
async function slackTextMessageChannel(channelID, text, installation){
  return await app.client.chat.postMessage({
    token:installation.bot.token,
    text:text,
    channel:channelID
  })
}

// message a user or channel with blocks
async function slackBlocksMessageChannel(channelID, blocks, installation){
  return await app.client.chat.postMessage({
    token:installation.bot.token,
    blocks:blocks,
    channel:channelID
  })
}

async function slackUpdateKnowaboutsChannel(knowaboutUserID, teamID, knowaboutID, originChannelID, installation){
  const doc = await db.collection('teams').doc(teamID).collection('users').doc(knowaboutUserID).collection('knowabouts').doc(knowaboutID).get()
  if(!doc.exists){
    console.log(`${knowaboutID} knowabout doc not found for user ${knowaboutUserID}`)
    return false
  }

  // paginate through conversations
  let conversations = {
    response_metadata:{
      next_cursor:'initial'
    }
  }
  // conversations.response_metadata = {}
  // conversations.response_metadata.next_cursor = 'initial'
  let channelID = null
  while(conversations.response_metadata.next_cursor){
    const params = {
      token:installation.bot.token,
      // limit:2, // for testing in low channel environments
    }
    // paginate
    if(conversations.response_metadata.next_cursor != 'initial')
      params.cursor = conversations.response_metadata.next_cursor

    conversations = await app.client.conversations.list(params)
    // console.log("conversations: ", conversations)

    for(const channel of conversations.channels){
      // console.log("channel: ", channel.name)
      // if(channel.name == 'knowabouts'){
      if(channel.name == 'knowabouts'){
        channelID = channel.id
        break
      }
    }
    console.log("next_cursor: ", conversations.response_metadata.next_cursor)
  }
  const slackResponse = await app.client.chat.postMessage({
    token:installation.bot.token,
    channel: channelID,
    text: `<@${doc.data().senderID}> gave <@${knowaboutUserID}> a Knowabout:\n${doc.data().text}`
  })

  console.log('------slackResponse: ', slackResponse)
  return slackResponse

  // disabled for now
  //// also message origin channel if it starts with C/public channel
  //if(originChannelID.startsWith('C') && originChannelID != channelID){
  //  await app.client.conversations.join({
  //    token:installation.bot.token,
  //    channel:originChannelID
  //  })
  //  await app.client.chat.postMessage({
  //    token:installation.bot.token,
  //    channel: originChannelID,
  //    text: `<@${doc.data().senderID}> gave <@${knowaboutUserID}> a Knowabout:\n${doc.data().text}`
  //  })
  //}
}

async function slackGetChannelID(name, installation){
  let conversations = {
    response_metadata:{
      next_cursor:'initial'
    }
  }
  // conversations.response_metadata = {}
  // conversations.response_metadata.next_cursor = 'initial'
  let channelID = null
  while(conversations.response_metadata.next_cursor){
    const params = {
      token:installation.bot.token,
      // limit:2, // for testing in low channel environments
    }
    // paginate
    if(conversations.response_metadata.next_cursor != 'initial')
      params.cursor = conversations.response_metadata.next_cursor

    conversations = await app.client.conversations.list(params)
    // console.log("conversations: ", conversations)

    for(const channel of conversations.channels){
      // console.log("channel: ", channel.name)
      // if(channel.name == 'knowabouts'){
      if(channel.name == name){
        channelID = channel.id
        console.log("found channel: ", channel.name)
        break
      }
    }
    console.log("next_cursor: ", conversations.response_metadata.next_cursor)
  }
  return channelID
}

// finds the most popular knowabout messages in #knowabouts
async function slackGetTop10Knowabouts(data, command, respond){
  const installation = data.installation
  const channelID = await slackGetChannelID('knowabouts', installation)
  const scoredMessages = []
  let messages = {
    response_metadata:{
      next_cursor:'initial'
    }
  }
  const oneWeekAgo = new Date()
  oneWeekAgo.setDate(oneWeekAgo.getDate()-7)

  while(messages.response_metadata.next_cursor){
    const params = {
      token:installation.bot.token,
      channel:channelID,
      oldest:oneWeekAgo.valueOf()/1000
    }
    // paginate
    if(messages.response_metadata.next_cursor != 'initial')
      params.cursor = messages.response_metadata.next_cursor

    messages = await app.client.conversations.history(params)

    // loop over messages and find ones that have a score
    for(const message of messages.messages){
      if((message.reactions || message.reply_count) && message.text.match('> gave <')){
        delete message.bot_profile
        const splits = message.ts.split('.')
        const date = new Date(Number(splits[0])*1000)
        message.date = date.toUTCString()
        message.score = 0
        if(message.reply_count)
          message.score += message.reply_count
        if(message.reactions){
          let tempTotal = 0
          for(const reaction of message.reactions){
            tempTotal += reaction.count
          }
          message.score+=tempTotal
        }
        // console.log('message: ', message)
        scoredMessages.push(message)
      }
    }
  }
  console.log('---------\n--------')
  scoredMessages.sort(function(a, b){return b.score - a.score})
  // console.log('sortedMessages: ', scoredMessages.slice(0, 15))
  let replyMessage = ''
  for(const message of scoredMessages.slice(0, 10)){
    const permalink = await app.client.chat.getPermalink({
      token:installation.bot.token,
      channel:channelID,
      message_ts:message.ts
    })
    // console.log('permalink: ', permalink)
    // console.log(`score: ${message.score} - ${message.date} - ${permalink.permalink}\n${message.text}\n`)
    replyMessage+=`${message.text}\n*score: ${message.score}* - ${message.date} - <${permalink.permalink}|link to conversation>\n\n`
  }
  respond(replyMessage)
}


//
// complex functions (slack and firestore or multiple APIs)
//

async function subscribeToKnowabouts(userID, teamID, installation){
  const knowaboutsChannelID = await slackGetChannelID('knowabouts', installation)
  const params = {
    token:installation.bot.token,
    channel: knowaboutsChannelID,
    users:userID
  }
  console.log('-----subscribeToKnowabouts params:', params)
  try{
    // add user to #knowabouts
    const slackResp = await app.client.conversations.invite(params)
  }catch(err){
    console.log('invite error: ', err)
  }
  console.log('after invite: ', userID)
  // add subscribed:true to userDoc
  const resp = await db.collection('teams').doc(teamID).collection('users').doc(userID).set({
    subscribed:true
  }, {merge:true})
  return true
}

async function unsubscribeToKnowabouts(userID, teamID, installation){
  const knowaboutsChannelID = await slackGetChannelID('knowabouts', installation)
  const params = {
    token:installation.bot.token,
    channel: knowaboutsChannelID,
    user:userID
  }
  console.log('-----unsubscribeToKnowabouts params:', params)
  // kick user from #knowabouts
  const slackResp = await app.client.conversations.kick(params)
}

// posts to knowabouts a pileon message and udpates their user doc in firestore
async function pileonUser(userID, teamID, installation){
  const resp = await db.collection('teams').doc(teamID).collection('users').doc(userID).set({
    pileon:true
  }, {merge:true})
  const channelID = await slackGetChannelID('knowabouts', installation)

  await app.client.chat.postMessage({
    token:installation.bot.token,
    channel: channelID,
    text:`<@${userID}> just received their first Knowabout. What else should we know about <@${userID}>? Welcome them to the party by giving them your own knowabout!`
  })
}

//flow for seeing sent knowabouts of username
async function sentKnowabouts(data, command, respond){
  const teamID = command.team_id
  const installation = data.installation
  const senderUsername = data.username
  const senderID = await slackGetUserID(senderUsername, installation)

  console.log(`getting sent knowabouts for user: ${senderUsername}:${senderID} on ${teamID}`)
  const knowabouts = await getSentKnowabouts(senderID, teamID)

  // build knowabout message string
  let knowaboutsString = `Viewing knowabouts sent by <@${senderID}>\n`
  let count = 0
  for(const knowabout of knowabouts){
    const splits = knowabout.ref.path.split('/')
    const knowaboutUserID = splits[3]
    knowaboutsString += `<@${knowabout.data().senderID}> gave <@${knowaboutUserID}> a Knowabout: "${knowabout.data().text}"\n`
    // only show 15 knowabouts 
    if(++count > 15)
      break

  }
  await respond(knowaboutsString)
}


// flow for deleting sent knowabouts
async function deleteSentKnowabouts(data, command, respond){
  const userID = command.user_id
  const teamID = command.team_id
  console.log(`getting sent knowabouts for user: ${userID} on ${teamID}`)
  const knowabouts = await getSentKnowabouts(userID, teamID)
  const blocks = getBlocksSentDeleteList(knowabouts)
  await respond({blocks})
}

// matches users
async function matchKnowabouts(data, command, respond){
  const teamID = command.team_id
  const installation = data.installation
  const userID1 = await slackGetUserID(data.username1, installation)
  const userID2 = await slackGetUserID(data.username2, installation)

  // sort userID for storing in firestore
  let smallUser = userID1
  let bigUser = userID2
  if(userID1 > userID2){
    smallUser = userID2
    bigUser = userID1
  }
  // see if match already exists then return early
  const matchDoc = await getMatch(userID1, userID2, teamID)
  if(matchDoc)
    return await respond(`<@${userID1}> has already been matched with <@${userID2}>`)

  // store in matches
  const addMatchRef = await db.collection('teams').doc(teamID).collection('matches').add({
    teamID:teamID,
    smallUser:smallUser,
    bigUser:bigUser,
    smallSentCount:0,
    bigSentCount:0,
    allSent:false,
    reminded:false,
    active:true,
    created:Timestamp.now()
  })

  await respond(`<@${userID1}> has been matched with <@${userID2}>`)
}

// gets stats on current matches
async function getMatchStats(data, command, respond){
  const teamID = command.team_id
  const installation = data.installation
  const oneWeekAgo = new Date()
  oneWeekAgo.setDate(oneWeekAgo.getDate()-7)
  const matches = await getMatches(teamID, oneWeekAgo)

  let statsMessage = `Stats on matches since ${oneWeekAgo}\n` 
  for(const match of matches){
    const matchData = match.data()
    statsMessage += `<@${matchData.bigUser}> ${matchData.bigSentCount} knowabouts written, <@${matchData.smallUser}> ${matchData.smallSentCount} knowabouts written\n`
    
  }
  await respond(statsMessage)
}

// clears matches and sends held knowabouts
async function clearMatches(data, command, respond){
  // get teams
  teamSnap = await db.collection('teams').get()
  for(const team of teamSnap.docs){
    const teamID = team.ref.id
    const installation = await app.receiver.installer.installationStore.fetchInstallation({teamId:teamID})

    // get active matches
    const snapshot = await db.collection('teams').doc(teamID).collection('matches').where('active', '==', true).get()

    // make all inactive
    for(const match of snapshot.docs){
      const matchData = match.data()
      const updateObj = {
        allSent:true,
        active:false
      }

      // send knowabouts if allSent is false
      if(!matchData.allSent){
        await sendAllMatchKnowabouts(matchData.smallUser, matchData.bigUser, teamID, installation)
        await sendAllMatchKnowabouts(matchData.bigUser, matchData.smallUser, teamID, installation)
      }
      await db.collection('teams').doc(teamID).collection('matches').doc(match.ref.id).update(updateObj)
    }
  }

}

// optin message in #general
async function beginMatch(data, command, respond){
  const teamID = command.team_id
  const installation = data.installation
  // create new beginMatch doc
  const addOptinRef = await db.collection('teams').doc(teamID).collection('matchoptins').add({
    yes:[],
    yesNames:[],
    skip:[],
    created:Timestamp.now()
  })
  const optinID = addOptinRef.id
  console.log("optinID: ", optinID)
  const optinDoc = await db.collection('teams').doc(teamID).collection('matchoptins').doc(optinID).get()
  const storedTimestamp = optinDoc.data().created.toDate()
  const teamDoc = await db.collection('teams').doc(teamID).get()
  const spreadsheet = teamDoc.data().spreadsheet

  // create new sheet
  await sheets.createSheet(spreadsheet, storedTimestamp)

  // join general
  const channelID = await slackGetChannelID('general', installation)
  try{
    await app.client.conversations.join({
      token:installation.bot.token,
      channel:channelID
    })
  }catch(err){
    console.log("join channel error: ", err)
  }
  // message general
  const blocks = getBlocksOptin(optinID)
  const optinMessage = await app.client.chat.postMessage({
    token:installation.bot.token,
    channel:channelID,
    blocks:blocks
  })

  const optinRef = await db.collection('teams').doc(teamID).collection('matchoptins').doc(optinID).update({messageTS:optinMessage.ts})
}


// sends all knowabouts for a match
async function sendAllMatchKnowabouts(knowaboutUserID, senderID, teamID, installation){
  // get all knowabouts sent to userID1 by userID2 and send them
  const docs = await getInactiveUserKnowaboutsBySender(knowaboutUserID, senderID, teamID)
  for(const knowabout of docs){
    const acceptRejectBlocks = getBlocksAcceptReject(senderID, knowabout.data().text, knowabout.id, null)

    // send accept/reject knowabout message
    await app.client.chat.postMessage({
      token:installation.bot.token,
      channel:knowaboutUserID,
      blocks:acceptRejectBlocks,
      text:`<@${senderID}> gave you a Knowabout:\n${knowabout.data().text}`
    })
  }
}

// sends a reminder to all matches
async function sendAllMatchReminders(){
  const knowaboutReq = 3
  // get teams
  teamSnap = await db.collection('teams').get()
  for(const team of teamSnap.docs){
    const teamID = team.ref.id
    const installation = await app.receiver.installer.installationStore.fetchInstallation({teamId:teamID})
    // get unreminded matches
    const matchSnap = await db.collection('teams').doc(teamID).collection('matches').where('reminded', '==', false).get()
    for(const match of matchSnap.docs){
      const matchData = match.data()
      if(matchData.smallSentCount == 0 && matchData.bigSentCount == 0){
        const blocksSmall = getBlocksNoMatchKnowabouts(matchData.bigUser, match.ref.id)
        const blocksBig = getBlocksNoMatchKnowabouts(matchData.smallUser, match.ref.id)
        await slackBlocksMessageChannel(matchData.smallUser, blocksSmall, installation)
        await slackBlocksMessageChannel(matchData.bigUser, blocksBig, installation)
      }else{
        // find which hole they fit in
        if(matchData.smallSentCount < knowaboutReq){
          const s = matchData.smallSentCount < knowaboutReq-1 ? 's' : ''
          if(matchData.smallSentCount == 0 && matchData.bigSentCount > 0){
            await slackTextMessageChannel(matchData.smallUser, `<@${matchData.bigUser}> has written Knowabouts for you. Write them ${knowaboutReq} Knowabouts to see the Knowabouts they wrote for you!`, installation)
          }else{
            await slackTextMessageChannel(matchData.smallUser, `To unlock the Knowabouts you received from <@${matchData.bigUser}>, write them ${knowaboutReq-matchData.smallSentCount} more Knowabout${s}!`, installation)
          }
        }
        if(matchData.bigSentCount < knowaboutReq){
          const s = matchData.bigSentCount < knowaboutReq ? 's' : ''
          if(matchData.bigSentCount == 0 && matchData.smallSentCount > 0){
            await slackTextMessageChannel(matchData.bigUser, `<@${matchData.smallUser}> has written Knowabouts for you. Write them ${knowaboutReq} Knowabouts to see the Knowabouts they wrote for you!`, installation)
          }else{
            await slackTextMessageChannel(matchData.bigUser, `To unlock the Knowabouts you received from <@${matchData.smallUser}>, write them ${knowaboutReq-matchData.bigSentCount} more Knowabout${s}!`, installation)
          }
        }
      }
      await match.ref.update({
        reminded:true
      })
    }
  }
}

// update slack profile 
// maybe not doable
async function updateSlackProfile(userID, teamID, installation){
  // get user knowabouts sorted desc by date
  const knowabouts = await getActiveKnowabouts(userID, teamID)

  // check if we have profile fields
  const teamDoc = await db.collection('teams').doc(teamID).get()
  
  const profile = await app.client.users.profile.get({
    token:installation.bot.token,
    include_labels:true
  })
  console.log('profile: ', profile)
  //await app.client.chat.postMessage({
}


//
// firestore functions
//

// test firestore
async function getFirestoreData(pendingKnowabout){
  // retrieve a pending knowabout
  const pendingRef = db.collection('pendingKnowabouts').doc(pendingKnowabout)
  const doc = await pendingRef.get()
  if(!doc.exists){
    console.log(`no document ${pendingKnowabout}`)
  }else{
    console.log(`${pendingKnowabout} data: `, doc.data())
  }
}

// store new pending knowabout for userID
async function newKnowabout(userID, name, teamID, text, senderID){
  // const pendingRe f = await db.collection('pendingKnowabouts').doc().set({
  //   userID:userID,
  //   text:text
  // })
  // console.log("pendingRef: ", pendingRef)
  // console.log("pendingRef.id: ", pendingRef.id)

  // add returns a documentreference and an id
  const addPendingRef = await db.collection('teams').doc(teamID).collection('users').doc(userID).collection('knowabouts').add({
    text:text,
    name:name,
    senderID:senderID,
    created:Timestamp.now(),
    active:false,
    teamID:teamID
  })
  // console.log("add pendingRef: ", addPendingRef)
  console.log("add pendingRef.id: ", addPendingRef.id)
  return addPendingRef.id
}

// sets know about to active:true
async function approveKnowabout(userID, teamID, pendingID){
  await db.collection('teams').doc(teamID).collection('users').doc(userID).collection('knowabouts').doc(pendingID).update({
    active:true
  })
  return true
}

// sets know about to active:false
async function disableKnowabout(userID, teamID, knowaboutID){
  await db.collection('teams').doc(teamID).collection('users').doc(userID).collection('knowabouts').doc(knowaboutID).update({
    active:false
  })
  return true
}

// gets knowabouts for user
async function getActiveKnowabouts(userID, teamID){
  // get active knowabouts ordered by created date
  const knowaboutsRef = await db.collection('teams').doc(teamID).collection('users').doc(userID).collection('knowabouts')
    .where('active', '==', true).orderBy('created', 'desc').get()
  for(const doc of knowaboutsRef.docs){
    // console.log('doc.data(): ', doc.data())
  }
  return knowaboutsRef.docs
}

// gets a knowabout
async function getKnowabout(userID, teamID, knowaboutID){
  const knowaboutDoc = await db.collection('teams').doc(teamID).collection('users').doc(userID).collection('knowabouts').doc(knowaboutID).get()
  return knowaboutDoc
}

// gets a knowabout sent by senderID
async function getInactiveUserKnowaboutsBySender(userID, senderID, teamID){
  const snapshot = await db.collection('teams').doc(teamID).collection('users').doc(userID).collection('knowabouts').where('senderID', '==', senderID).where('active', '==', false).get()
  return snapshot.docs

}

// gets a user's sent knowabouts
async function getSentKnowabouts(userID, teamID){
  const knowaboutsDoc = await db.collectionGroup('knowabouts').where('senderID', '==', userID).where('teamID', '==', teamID).where('active', '==', true).orderBy('created', 'desc').get()
  return knowaboutsDoc.docs
}


// search knowabouts for text
async function searchKnowabouts(data, command, respond){
  const results = await index.search(data.text, {
    advancedSyntax:true,
    filters:'active:true'
  })
  console.log("-----search response: ", results)

  const activeHits = results.hits.filter(h=>h.active)
  results.hits = activeHits
  const blocks = await getBlocksSearch(results, data.text)
  await respond({blocks:blocks})
}

// finds a match between two users
async function getMatch(userID1, userID2, teamID){
  let smallUser = userID1
  let bigUser = userID2
  if(userID1 > userID2){
    smallUser = userID2
    bigUser = userID1
  }
  const snapshot = await db.collection('teams').doc(teamID).collection('matches').where('smallUser', '==', smallUser).where('bigUser', '==', bigUser).get()
  console.log('------match snapshot: ', snapshot)
  if(snapshot.docs.length > 0)
    return snapshot.docs[0]
  return null
}

// finds a match between two users
async function getMatches(teamID, afterDate){
  const snapshot = await db.collection('teams').doc(teamID).collection('matches').where('created', '>=', afterDate).get()
  console.log('------matches snapshot: ', snapshot)
  return snapshot.docs
}

// update sent count for a match
async function updateMatchSent(knowaboutUserID, senderID, teamID){
  let smallUser = knowaboutUserID
  let bigUser = senderID
  let incrementUser = 'bigSentCount'
  const knowaboutsNeeded = 3
  if(knowaboutUserID > senderID){
    smallUser = senderID
    bigUser = knowaboutUserID
    incrementUser = 'smallSentCount'
  }
  updateObj = {}
  updateObj[incrementUser] = FieldValue.increment(1)

  const matchDoc = await getMatch(knowaboutUserID, senderID, teamID)
  if(!matchDoc)
    return null
  await matchDoc.ref.update(updateObj)
  
  let sendKnowabouts = false
  const matchData = matchDoc.data()
  // check if enough knowabouts have been written
  if(incrementUser == 'smallSentCount'){
    if(matchData.smallSentCount >= 2 && matchData.bigSentCount >= 3 && !matchData.allSent)
      sendKnowabouts = true
  }else{
    if(matchData.smallSentCount >= 3 && matchData.bigSentCount >= 2 && !matchData.allSent)
      sendKnowabouts = true
  }
  if(sendKnowabouts){
    await matchDoc.ref.update({allSent:true})
    return {sendAll:true}
  }
  if(matchData.allSent)
    return {allSent:true}
  return true
}

// update didn't meet flag
async function updateMatchDidNotMeet(teamID, matchID){
  const matchRef = await db.collection('teams').doc(teamID).collection('matches').doc(matchID).update({
    didNotMeet:true
  })
}

// update optin
async function updateOptin(teamID, optinID, user, yesSkip){
  const installation = await  app.receiver.installer.installationStore.fetchInstallation({teamId:teamID})
  const updateObj = {}
  if(yesSkip == 'yes'){
    updateObj.yes = FieldValue.arrayUnion(user.id)
    updateObj.yesNames = FieldValue.arrayUnion(user.real_name)
  }else{
    updateObj.skip = FieldValue.arrayUnion(user.id)
  }

  const matchRef = await db.collection('teams').doc(teamID).collection('matchoptins').doc(optinID).update(updateObj)

  const matchDoc = await db.collection('teams').doc(teamID).collection('matchoptins').doc(optinID).get()
  const docData = matchDoc.data()
  const teamDoc = await db.collection('teams').doc(teamID).get()
  const spreadsheet = teamDoc.data().spreadsheet

  // update google sheet
  const storedTimestamp = docData.created.toDate()
  await sheets.updateNames(spreadsheet, docData.yesNames, storedTimestamp)

  // update message 
  //const channelID = await slackGetChannelID('general', installation)
  //const blocks = getBlocksOptin(optinID)
  //const optinBlocks = getBlocksOptinUsers(docData)
  //await app.client.chat.update({
  //  token:installation.bot.token,
  //  channel:channelID,
  //  ts:docData.messageTS,
  //  blocks:blocks.concat(optinBlocks)
  //})
  // send DM to user
  let DMMessage = `Great! You're confirmed for a Knowabouts match this week. Stay tuned for your curated match!`
  if(yesSkip == 'skip')
    DMMessage = `Thanks for letting us know you're skipping this week of Knowabouts matches!`
  await app.client.chat.postMessage({
    token:installation.bot.token,
    channel:user.id,
    text:DMMessage
  })

}

// save optin choices
async function saveOptinChoices(user, teamID, choices){
  for(const choice of choices){
    const updateObj = {
      people:FieldValue.arrayUnion(user.id),
      peopleNames:FieldValue.arrayUnion(user.real_name),
    }
    const topicRef = await db.collection('teams').doc(teamID).collection('topics').doc(choice).update(updateObj)
  }
}

// gets user
async function getUserFirestore(userID, teamID){
  console.log(`user: ${userID}, team: ${teamID}`)
  const userDoc = await db.collection('teams').doc(teamID).collection('users').doc(userID).get()
  return userDoc
}

// gets all knowabouts in the last timeframe
async function getKnowaboutsTimeframe(teamID, after, before){
  console.log('getKnowaboutsTimeframe')
  const knowaboutsDoc = await db.collectionGroup('knowabouts').where('created', '>=', after).where('created', '<=', before).where('active', '==', true).where('teamID', '==', teamID).get()
  console.log(knowaboutsDoc)
  console.log(knowaboutsDoc.size)
  return knowaboutsDoc
}

// sends reminders to everyone that got a Knowabout in the last week
async function sendRemindersKnowabouts(){
  const oneWeekAgo = new Date()
  oneWeekAgo.setDate(oneWeekAgo.getDate()-7)
  const now = new Date()
  const teamID = 'TLN6SP0FM' // production
  // const teamID = 'T02SB19JVK6' // test
  const installation = await app.receiver.installer.installationStore.fetchInstallation({teamId:teamID})
  const knowabouts = await getKnowaboutsTimeframe(teamID, oneWeekAgo, now)
  const users = {}
  for(const i in knowabouts.docs){
    const knowabout = knowabouts.docs[i]
  
    // console.log('knowabout.data(): ', knowabout.data())
    console.log('knowabout.ref.path: ', knowabout.ref.path)
    const splits = knowabout.ref.path.split('/')
    const teamID = splits[1]
    const userID = splits[3]
    const senderID = knowabout.data().senderID
    // tally senderIDs for each user
    if(users[userID]){
      if(users[userID][senderID]){
        users[userID][senderID].count = users[userID][senderID].count+1
      }else{
        users[userID][senderID] = {count:1}
      }
    }else{
      users[userID] = {}
      users[userID][senderID] = {count:1}
    }
  }
  for(const key in users){
    const user = users[key]
    const userID = key
    // tally number of knowabouts per sender and build senders array
    let totalReceived = 0
    const senders = []
    for(const i in user){
      const sender = user[i]
      const senderID = i
      totalReceived += sender.count
      senders.push({senderID:senderID, count:sender.count})
    }
    senders.sort((a,b)=>b.count - a.count)
    // console.log('senders sorted: ', senders)
    
    // message the user with knowabout stats and reminder to send some
    let returnTheLove = `<@${userID}>, you received ${totalReceived} knowabouts this week from:\n`
    for(const sender of senders){
      returnTheLove+=`<@${sender.senderID}>: ${sender.count}\n`
    }
    returnTheLove+='Give them some Knowabouts to return the love!'
    console.log('returnTheLove: ', returnTheLove)
    await app.client.chat.postMessage({
      token:installation.bot.token,
      channel:userID,
      text: returnTheLove
    })
  }
  console.log('users: ', users)
  console.log('knowabouts size: ', knowabouts.size)

}

// prints leaderboard last week
async function sendLeaderboardLastWeek(data, command, respond){
  const now = new Date()
  const oneWeekAgo = new Date()
  oneWeekAgo.setDate(oneWeekAgo.getDate()-7)
  const leaderboardText = await getLeaderboard(oneWeekAgo, now)

  await respond(leaderboardText)
}

// gets leaderboard
async function getLeaderboard(after, before){
  const knowabouts = await getKnowaboutsTimeframe('TLN6SP0FM', after, before)
  const senders = {}
  knowabouts.forEach(knowabout=>{
    const data = knowabout.data()
    // increment sender counts
    if(senders[data.senderID]){
      senders[data.senderID] = senders[data.senderID] + 1
    }else{
      // initialize new sender
      senders[data.senderID] = 1
    }
  })
  // console.log('senders: ', senders)
  const sendersSort = []

  for(const sender in senders){
    sendersSort.push({key:sender, value:senders[sender]})
  }
  // console.log('sendersSort: ', sendersSort)
  sendersSort.sort(function (a, b){
    return b.value - a.value
  })
  console.log('sendersSort: ', sendersSort)

  const now = new Date()
  let leaderboard = 'Knowabouts leaderboard for the week of '+now.toDateString()+"\n"
  let count = 1
  for(const sender of sendersSort){
    leaderboard += `${count++}. ${sender.value} - <@${sender.key}>\n`
  }
  console.log(leaderboard)
  return leaderboard
}

// updates knowabouts with their teamID
// admin util function
async function updateKnowaboutsTeamID(){
  const oneWeekAgo = new Date()
  oneWeekAgo.setDate(oneWeekAgo.getDate()-1)
  const now = new Date()
  // const knowabouts = await db.collectionGroup('knowabouts').where('created', '>=', oneWeekAgo).where('created', '<=', now).where('active', '==', true).get()
  const knowabouts = await db.collectionGroup('knowabouts').where('active', '==', true).get()
  // knowabouts.forEach(knowabout=>{
  for(const i in knowabouts.docs){
    const knowabout = knowabouts.docs[i]
    // console.log('knowabout: ', knowabout)
    // filter on those in teams path
    if(knowabout.ref.path.match('teams')){
      console.log('knowabout.data(): ', knowabout.data())
      console.log('knowabout.ref.path: ', knowabout.ref.path)
      const splits = knowabout.ref.path.split('/')
      if(!knowabout.data().teamID){
        console.log('missing teamID: ', splits[1])
        await knowabout.ref.update({teamID:splits[1]})
      }
    }
  }
  console.log('knowabouts size: ', knowabouts.size)
}

// admin util function that marks people as piledon if they have an accepted knowabout
async function markAsPiledOnAdmin(){
  const oneWeekAgo = new Date()
  oneWeekAgo.setDate(oneWeekAgo.getDate()-1)
  const now = new Date()
  // const knowabouts = await db.collectionGroup('knowabouts').where('created', '>=', oneWeekAgo).where('created', '<=', now).where('active', '==', true).get()
  const knowabouts = await db.collectionGroup('knowabouts').where('active', '==', true).get()
  const usersWithActives = {}
  // knowabouts.forEach(knowabout=>{
  for(const i in knowabouts.docs){
    const knowabout = knowabouts.docs[i]
    
    // filter on those in teams path
    if(knowabout.ref.path.match('teams')){
      // console.log('knowabout.data(): ', knowabout.data())
      console.log('knowabout.ref.path: ', knowabout.ref.path)
      const splits = knowabout.ref.path.split('/')
      const teamID = splits[1]
      const userID = splits[3]
      if(usersWithActives[userID]){
        usersWithActives[userID].count = usersWithActives[userID].count+1
      }else{
        usersWithActives[userID] = {teamID: teamID, count:1}
      }
      // const resp = await db.collection('teams').doc(teamID).collection('users').doc(userID).set({
      //   pileon:true
      // }, {merge:true})
      // if(!knowabout.data().teamID){
      //   console.log('missing teamID: ', splits[1])
      //   await knowabout.ref.update({teamID:splits[1]})
      // }
    }
  }
  for(const key in usersWithActives){
    const user = usersWithActives[key]
    const userID = key
    const teamID = user.teamID
    console.log(`updating user: ${key}, teamID: ${user.teamID} as piledon`)
    const resp = await db.collection('teams').doc(teamID).collection('users').doc(userID).set({
      pileon:true
    }, {merge:true})
  }
  console.log('usersWithActives: ', usersWithActives)
  console.log('knowabouts size: ', knowabouts.size)
}

// admin util date formatting
async function testGetDate(){
  const knowabout = await getKnowabout('U02S5QBLBC5', 'T02SB19JVK6', '0xZJJ4sgnfNgn6BBC0gV')
  console.log('knowabout: ', knowabout)
  console.log('knowabout.data(): ', knowabout.data())
  console.log('knowabout.data().created: ', knowabout.data().created)
  const storedTimestamp = knowabout.data().created.toDate()
  console.log('storedTimestamp: ', storedTimestamp)
  const dayTS = dayjs(storedTimestamp)
  console.log(dayTS.format('MMM D, YYYY'))
}

// admin insert topics
async function insertTopics(){
  //const teamID = 'T02SB19JVK6' // beta
  const teamID = 'TLN6SP0FM' // production
  const topics = ["Hawaii", "Twitter reading group", "marathon training", "making pizza", "buying houses", "surfing", "Pittsburgh", "Florida", "beaches", "vegan recipes", "organizing bookshelves", "Yankees", "job hunting", "apartment hunting", "living in Sausalito", "living in Tahoe", "learning to ski", "data science", "ETL pipelines", "ML models", "sales at early stage startup", "community building", "combatting insomnia", "fintech", "Latinx founders", "sailing", "work-life balance", "solo founder", "Miami", "8-bit games", "Oklahoma", "traditional Chinese medicine", "DAOs", "Japanese cooking", "MBA interviews", "turning trash into valuable metals", "NYC bedbug registry", "Dutch", "urban rooftop farms", "satellite design", "The Dawn of Everything", "ethnic chefs", "Shrooms", "NYT crossword", "whirlwind romance", "dressage", "living in Norway", "cross-generational parties", "climate tech", "professional poker", "pickleball", "AR/e-commerce", "golf", "Northern lights", "HGTV renovation", "Defi lending", "boardgame geek", "blacksmithing", "adventure buddies", "freestyle swimming", "pilot training", "Puerto Rico", "baking"]
  for(const topic of topics){
    const lower = topic.toLowerCase().replace("/", "\\")
    const doc = await db.collection('teams').doc(teamID).collection('topics').doc(lower).get()
    if(doc.exists){
      console.log(`${topic} already exists`)
    }else{
      console.log(`inserting: ${topic}`)
      await db.collection('teams').doc(teamID).collection('topics').doc(lower).set({
        text:topic,
        people:[],
        created:Timestamp.now(),
      })
    }
  }
}

// admin copy topics to team doc
async function copyTopics(){
  // get teams
  teamSnap = await db.collection('teams').get()
  for(const team of teamSnap.docs){
    const teamID = team.ref.id
    // get topics for a team and store them in team doc
    const topicSnap = await db.collection('teams').doc(teamID).collection('topics').get()
    let topics = []
    for(const topic of topicSnap.docs){
      const topicData = topic.data()
      topics.push(topicData.text)
    }
    await db.collection('teams').doc(teamID).update({
      topics:topics
    })
  }
}

// store credentials in firestore
async function storeCredentials(teamID, installation){
  const setRef = await db.collection('teams').doc(teamID).set(installation)
  // console.log('set ref: ', setRef)
  // const doc = await db.collection('teams').doc(teamID).get()
  // console.log('doc.data(): ', doc.data())
}

// fetch credentials from firestore
async function getCredentials(teamID){
  const doc = await db.collection('teams').doc(teamID).get()
  // console.log('doc.data(): ', doc.data())
  return doc.data()
}


const sendAppRequest = () => {
  console.log("Stayin alive");
  fetch("https://knowabout.uw.r.appspot.com/slack/install");
};
// only keep alive on gcloud
if(process.env.ENVIRONMENT == 'gcloud'){
  console.log('stay alive active')
  const intervalId = setInterval(sendAppRequest, 60000);
}


// cron jobs
// friday leaderboard
cron.schedule('0 12 * * Friday', async ()=>{
  // TODO: do it for all teams
  const teamID = 'TLN6SP0FM'
  const installation = await app.receiver.installer.installationStore.fetchInstallation({teamId:teamID})
  // const channelID = await slackGetChannelID('random-dev', installation)
  const channelID = await slackGetChannelID('knowabouts', installation)
  const oneWeekAgo = new Date()
  oneWeekAgo.setDate(oneWeekAgo.getDate()-7)
  const now = new Date()

  const leaderboardText = await getLeaderboard(oneWeekAgo, now)

  await app.client.chat.postMessage({
    token:installation.bot.token,
    channel:channelID,
    text:leaderboardText
  })
}, {timezone:'America/Los_Angeles'})

// return the love reminders
cron.schedule('0 9 * * Friday', async ()=>{
  // TODO: do it for all teams
  await sendRemindersKnowabouts()

}, {timezone:'America/Los_Angeles'})

// match knowabout reminders
cron.schedule('0 12 * * Sunday', async ()=>{
  await sendAllMatchReminders()

}, {timezone:'America/Los_Angeles'})

// clear matches and send knowabouts
cron.schedule('0 18 * * Sunday', async ()=>{
  await clearMatches()

}, {timezone:'America/Los_Angeles'})


// test functions without deploy
//console.log('admin tests are active');
//(async () => {
//  // await getKnowaboutsTimeframe('TLN6SP0FM')
//  // await sendLeaderboardLastWeek({}, 'test', {})
//  // const installation = await app.receiver.installer.installationStore.fetchInstallation({teamId:teamID})
//  // await slackGetTop10Knowabouts({installation}, 'test', {})
//  // await updateKnowaboutsTeamID() // in algolia now
//  // const sleep = (waitTimeInMs) => new Promise(resolve => setTimeout(resolve, waitTimeInMs))
//  // await sleep(5000)
//  // await markAsPiledOnAdmin() 
//  // await sendRemindersKnowabouts()
//  //const userID = 'U02S212AJ1H'
//  //const teamID = 'TLN6SP0FM'
//  //await deleteSentKnowabouts({}, {team_id:teamID, user_id:userID}, {})
//  //await testGetDate()
//  //const matchDoc = await getMatch('U02S8MZH0G2', 'U02S5QBLBC5', 'T02SB19JVK6')
//  //if(matchDoc){
//  //  console.log("matchDoc: ", matchDoc)
//  //  console.log("matchDoc.data(): ", matchDoc.data())
//  //}
//  //await sheets.createSheet()
//  //const installation = await app.receiver.installer.installationStore.fetchInstallation({teamId:'T02SB19JVK6'})
//  //await updateSlackProfile('U02S8MZH0G2', 'T02SB19JVK6', installation)
//
//  await insertTopics()
//  await copyTopics()
//  console.log('got knowabouts');
//})();

// console.log("process.env: ", process.env)
