from fabric.api import local, run
from fabric.context_managers import cd



def heroku(commit="default"):
    local("git add .")
    local("git commit -m \"%s\"" % commit)
    local("git push")
    local("git push heroku master")

def gcloud(commit="default"):
    local("git add .")
    local("git commit -m \"%s\"" % commit)
    local("git push")
    local("gcloud app deploy")

def herokubare():
    local("git push heroku master")

def lazygit(commit="default"):
    local("git add -A")
    local("git commit -m \"%s\"" % commit)
    local("git push")

